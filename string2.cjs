// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and 
// return it in an array in numeric values.
// [111, 139, 161, 143].Support IPV4 character only i.e. from 0.0.0.0 to 255.255.255.255

const split=(address)=>{
    var parts=address.split(".");
    if(parts.length!==4) return []; 
    for (part of parts){
        let partNum=Number(part)
        if (isNaN(partNum) || partNum<0 || partNum>255) return []; //check for ipv 
    }
    return parts.map((e)=>Number(e));
    }



module.exports=split;