// / ==== String Problem #3 ====
//  Given a string in the format of "10/1/2021", print the month in which the date is present in.

const months={
    1:"January",
    2:"February",
    3:"March",
    4:"April",
    5:"May",
    6:"June",
    7:"July",
    8:"August",
    9:"September",
    10:"October",
    11:"November",
    12:"December"
}

const monthFromDate=(date)=>{
    var splitDate=date.split("/");
    return months[splitDate[1]];
}

module.exports=monthFromDate;