// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

const fullName=(object)=>{
var nameArray = [];
for (var key in object) {
  nameArray.push(object[key]);
  }
let lowerArray=nameArray.map((e)=>{
    let lower=e.toLowerCase();
    return lower.charAt(0).toUpperCase()+lower.slice(1);
})
 let titleCase=lowerArray.join(" ")
 return(titleCase)
}

module.exports=fullName;