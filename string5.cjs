// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.


const arrayToString=(string)=>{  // even if array is empty, it will not join anything and hence return empty string only.No need to add case
    return string.join(" ")
}


module.exports=arrayToString;


