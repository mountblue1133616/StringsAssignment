function toNumber(string){
    const withoutSpecialChar =string.replace('$',"").replace(',',"")  //removes dollar and comma
    const number=Number(withoutSpecialChar)
    if(!isNaN(number)){  //checks if number is valid
        return number;
    }else{
        return 0;
    }
}

module.exports = toNumber;
